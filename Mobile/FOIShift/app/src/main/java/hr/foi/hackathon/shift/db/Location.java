package hr.foi.hackathon.shift.db;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by ahuskano on 6/2/2014.
 */
@Table(name="Location")
public class Location extends Model {

    @Column(name="realID")
    private long realID;

    @Column(name="country")
    private String country;

    @Column(name="city")
    private String city;

    @Column(name="isCapital")
    private boolean isCapital;

    @Column(name="latitudeDD")
    private double latitudeDD;

    @Column(name="longitudeDD")
    private double longitudeDD;

    @Column(name="locationId")
    private long locationId;

    public long getRealID() {
        return realID;
    }

    public void setRealID(long realID) {
        this.realID = realID;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isCapital() {
        return isCapital;
    }

    public void setCapital(boolean isCapital) {
        this.isCapital = isCapital;
    }

    public double getLatitudeDD() {
        return latitudeDD;
    }

    public void setLatitudeDD(double latitudeDD) {
        this.latitudeDD = latitudeDD;
    }

    public double getLongitudeDD() {
        return longitudeDD;
    }

    public void setLongitudeDD(double longitudeDD) {
        this.longitudeDD = longitudeDD;
    }

    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public Location() {
    }

    public Location(long realID, String country, String city, boolean isCapital, double latitudeDD, double longitudeDD, long locationId) {
        this.realID = realID;
        this.country = country;
        this.city = city;
        this.isCapital = isCapital;
        this.latitudeDD = latitudeDD;
        this.longitudeDD = longitudeDD;
        this.locationId = locationId;
    }
}
