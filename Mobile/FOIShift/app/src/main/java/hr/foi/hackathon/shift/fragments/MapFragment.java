package hr.foi.hackathon.shift.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import hr.foi.hackathon.shift.IncidentDetailActivity;
import hr.foi.hackathon.shift.R;
import hr.foi.hackathon.shift.util.Data;

/**
 * Created by ahuskano on 6/2/2014.
 */
public class MapFragment extends SherlockFragment {
    private GoogleMap map = null;
    private View view = null;
    private LatLng myLocation = null;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {
            view = inflater
                    .inflate(R.layout.fragment_map, container, false);
            setUpMapIfNeeded();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void setUpMapIfNeeded() {
        if (map == null) {
            map = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            if (map == null) {
            }
            map.setMyLocationEnabled(true);
            addMarkers();
            setUp();
            if (map != null) {
            }
        }
    }

    private void setUp() {
        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                LayoutInflater inflater = (LayoutInflater) getSherlockActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = inflater.inflate(R.layout.marker, null);
                TextView title = (TextView) v.findViewById(R.id.tvTitle);
                title.setText(marker.getTitle());
                return v;
            }
        });
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.showInfoWindow();
                return true;
            }
        });
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                marker.hideInfoWindow();
                IncidentsFragment.incident = Integer.valueOf(marker.getSnippet());
                startActivity(new Intent(getSherlockActivity(), IncidentDetailActivity.class));

            }
        });
    }


    private void addMarkers() {
        if (Data.incidentResponse != null) {
            for (int i = 0; i < Data.incidentResponse.length; i++) {
                map.addMarker(new MarkerOptions().position(new LatLng(Data.incidentResponse[i].getLat(), Data.incidentResponse[i].getLng())).title(Data.incidentResponse[i].getIncidentLabel()).snippet(String.valueOf(Data.incidentResponse[i].getIncidentId())));
            }
        }
    }
}
