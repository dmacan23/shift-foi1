package hr.foi.hackathon.shift.types;

import com.google.gson.annotations.SerializedName;

import hr.foi.hackathon.shift.util.Const;

/**
 * Created by David on 2.6.2014..
 */
public class SubmitIncident {

    @SerializedName(Const.SI_USERID)
    private long userId;
    @SerializedName(Const.SI_LATTITUDE)
    private double latitude = 45.7221489;
    @SerializedName(Const.SI_LONGITUDE)
    private double longitude = 17.4077368;
    @SerializedName(Const.SI_TITLE)
    private String title;
    @SerializedName(Const.SI_DESC)
    private String description;
    @SerializedName(Const.SI_COST)
    private int cost;
    @SerializedName(Const.SI_CURRENCY)
    private String currency;
    @SerializedName(Const.SI_COST_DESC)
    private String costDescription;
    @SerializedName(Const.SI_MC_INJ)
    private int mcInjuries;
    @SerializedName(Const.SI_NMC_INJ)
    private int nmcInjuries;
    @SerializedName(Const.SI_MC_DTH)
    private int mcDeaths;
    @SerializedName(Const.SI_NMC_DTH)
    private int nmcDeaths;
    @SerializedName(Const.SI_VIEW_LVL)
    private long viewLevelId;
    @SerializedName(Const.SI_PRIM_CAT)
    private long primaryCategoryId;
    @SerializedName(Const.SI_SEC_CAT)
    private long secondaryCategoryId;
    @SerializedName(Const.SI_OUTCOME)
    private long outcomeId;
    @SerializedName(Const.SI_TYPE)
    private long typeId;
    @SerializedName(Const.SI_CITY)
    private String cityName;
    @SerializedName(Const.SI_COUNTRY)
    private String countryName;
    @SerializedName("Media")
    private String media;


    public SubmitIncident() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCostDescription() {
        return costDescription;
    }

    public void setCostDescription(String costDescription) {
        this.costDescription = costDescription;
    }

    public int getMcInjuries() {
        return mcInjuries;
    }

    public void setMcInjuries(int mcInjuries) {
        this.mcInjuries = mcInjuries;
    }

    public int getNmcInjuries() {
        return nmcInjuries;
    }

    public void setNmcInjuries(int nmcInjuries) {
        this.nmcInjuries = nmcInjuries;
    }

    public int getMcDeaths() {
        return mcDeaths;
    }

    public void setMcDeaths(int mcDeaths) {
        this.mcDeaths = mcDeaths;
    }

    public int getNmcDeaths() {
        return nmcDeaths;
    }

    public void setNmcDeaths(int nmcDeaths) {
        this.nmcDeaths = nmcDeaths;
    }

    public long getViewLevelId() {
        return viewLevelId;
    }

    public void setViewLevelId(long viewLevelId) {
        this.viewLevelId = viewLevelId;
    }

    public long getPrimaryCategoryId() {
        return primaryCategoryId;
    }

    public void setPrimaryCategoryId(long primaryCategoryId) {
        this.primaryCategoryId = primaryCategoryId;
    }

    public long getSecondaryCategoryId() {
        return secondaryCategoryId;
    }

    public void setSecondaryCategoryId(long secondaryCategoryId) {
        this.secondaryCategoryId = secondaryCategoryId;
    }

    public long getOutcomeId() {
        return outcomeId;
    }

    public void setOutcomeId(long outcomeId) {
        this.outcomeId = outcomeId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }
}
