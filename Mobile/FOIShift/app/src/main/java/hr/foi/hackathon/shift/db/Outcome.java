package hr.foi.hackathon.shift.db;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by ahuskano on 6/2/2014.
 */
@Table(name="Outcome")
public class Outcome extends Model {
    @Column(name="realID")
    private long realID;

    @Column(name="name")
    private String name;

    public Outcome(long realID, String name) {
        this.realID = realID;
        this.name = name;
    }

    public Outcome() {
    }

    public long getRealID() {
        return realID;
    }

    public void setRealID(long realID) {
        this.realID = realID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
