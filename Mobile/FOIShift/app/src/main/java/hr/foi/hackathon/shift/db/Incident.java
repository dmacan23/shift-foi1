package hr.foi.hackathon.shift.db;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by ahuskano on 6/2/2014.
 */
@Table(name = "Incident")
public class Incident extends Model {
    @Column(name = "realID")
    private long realID;

    @Column(name = "userId")
    private User userId;

    @Column(name = "statusId")
    private Status statusId;

    @Column(name="viewLevelId")
    private ViewLevel viewLevelId;

    @Column(name="title")
    private String title;

    @Column(name="incidentDate")
    private String incidentDate;

    @Column(name="incidentLocationId")
    private Location incidentLocationId;

    @Column(name="typeId")
    private Type typeId;

    @Column(name="primaryCategoryId")
    private Category primaryCategoryId;

    @Column(name="secondaryCategoryId")
    private Category secondaryCategoryId;

    @Column(name="outcomeId")
    private Outcome outcomeId;

    @Column(name="description")
    private String description;

    @Column(name="incidentLossCost")
    private double incidentLossCost;

    @Column(name="incidentLossDescription")
    private String incidentLossDescription;

    @Column(name="numberOfIMCInjuries")
    private int numberOfIMCInjuries;

    @Column(name="numberOfNonIMCInjuries")
    private int numberOfNonIMCInjuries;

    @Column(name="numberOfIMDeaths")
    private int numberOfIMDeaths;

    @Column(name="numberOfNonIMCDeaths")
    private int numberOfNonIMCDeaths;

    @Column(name="multipleIncidentsDescription")
    private String multipleIncidentsDescription;

    @Column(name="relateIncidentsDescription")
    private String relateIncidentsDescription;

    public long getRealID() {
        return realID;
    }

    public void setRealID(long realID) {
        this.realID = realID;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Status getStatusId() {
        return statusId;
    }

    public void setStatusId(Status statusId) {
        this.statusId = statusId;
    }

    public ViewLevel getViewLevelId() {
        return viewLevelId;
    }

    public void setViewLevelId(ViewLevel viewLevelId) {
        this.viewLevelId = viewLevelId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIncidentDate() {
        return incidentDate;
    }

    public void setIncidentDate(String incidentDate) {
        this.incidentDate = incidentDate;
    }

    public Location getIncidentLocationId() {
        return incidentLocationId;
    }

    public void setIncidentLocationId(Location incidentLocationId) {
        this.incidentLocationId = incidentLocationId;
    }

    public Type getTypeId() {
        return typeId;
    }

    public void setTypeId(Type typeId) {
        this.typeId = typeId;
    }

    public Category getPrimaryCategoryId() {
        return primaryCategoryId;
    }

    public void setPrimaryCategoryId(Category primaryCategoryId) {
        this.primaryCategoryId = primaryCategoryId;
    }

    public Category getSecondaryCategoryId() {
        return secondaryCategoryId;
    }

    public void setSecondaryCategoryId(Category secondaryCategoryId) {
        this.secondaryCategoryId = secondaryCategoryId;
    }

    public Outcome getOutcomeId() {
        return outcomeId;
    }

    public void setOutcomeId(Outcome outcomeId) {
        this.outcomeId = outcomeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getIncidentLossCost() {
        return incidentLossCost;
    }

    public void setIncidentLossCost(double incidentLossCost) {
        this.incidentLossCost = incidentLossCost;
    }

    public String getIncidentLossDescription() {
        return incidentLossDescription;
    }

    public void setIncidentLossDescription(String incidentLossDescription) {
        this.incidentLossDescription = incidentLossDescription;
    }

    public int getNumberOfIMCInjuries() {
        return numberOfIMCInjuries;
    }

    public void setNumberOfIMCInjuries(int numberOfIMCInjuries) {
        this.numberOfIMCInjuries = numberOfIMCInjuries;
    }

    public int getNumberOfNonIMCInjuries() {
        return numberOfNonIMCInjuries;
    }

    public void setNumberOfNonIMCInjuries(int numberOfNonIMCInjuries) {
        this.numberOfNonIMCInjuries = numberOfNonIMCInjuries;
    }

    public int getNumberOfIMDeaths() {
        return numberOfIMDeaths;
    }

    public void setNumberOfIMDeaths(int numberOfIMDeaths) {
        this.numberOfIMDeaths = numberOfIMDeaths;
    }

    public int getNumberOfNonIMCDeaths() {
        return numberOfNonIMCDeaths;
    }

    public void setNumberOfNonIMCDeaths(int numberOfNonIMCDeaths) {
        this.numberOfNonIMCDeaths = numberOfNonIMCDeaths;
    }

    public String getMultipleIncidentsDescription() {
        return multipleIncidentsDescription;
    }

    public void setMultipleIncidentsDescription(String multipleIncidentsDescription) {
        this.multipleIncidentsDescription = multipleIncidentsDescription;
    }

    public String getRelateIncidentsDescription() {
        return relateIncidentsDescription;
    }

    public void setRelateIncidentsDescription(String relateIncidentsDescription) {
        this.relateIncidentsDescription = relateIncidentsDescription;
    }

    public Incident() {
    }

    public Incident(long realID, User userId, Status statusId, ViewLevel viewLevelId, String title, String incidentDate, Location incidentLocationId, Type typeId, Category primaryCategoryId, Category secondaryCategoryId, Outcome outcomeId, String description, double incidentLossCost, String incidentLossDescription, int numberOfIMCInjuries, int numberOfNonIMCInjuries, int numberOfIMDeaths, int numberOfNonIMCDeaths, String multipleIncidentsDescription, String relateIncidentsDescription) {
        this.realID = realID;
        this.userId = userId;
        this.statusId = statusId;
        this.viewLevelId = viewLevelId;
        this.title = title;
        this.incidentDate = incidentDate;
        this.incidentLocationId = incidentLocationId;
        this.typeId = typeId;
        this.primaryCategoryId = primaryCategoryId;
        this.secondaryCategoryId = secondaryCategoryId;
        this.outcomeId = outcomeId;
        this.description = description;
        this.incidentLossCost = incidentLossCost;
        this.incidentLossDescription = incidentLossDescription;
        this.numberOfIMCInjuries = numberOfIMCInjuries;
        this.numberOfNonIMCInjuries = numberOfNonIMCInjuries;
        this.numberOfIMDeaths = numberOfIMDeaths;
        this.numberOfNonIMCDeaths = numberOfNonIMCDeaths;
        this.multipleIncidentsDescription = multipleIncidentsDescription;
        this.relateIncidentsDescription = relateIncidentsDescription;
    }
}
