package hr.foi.hackathon.shift.views;

import com.google.gson.annotations.SerializedName;

import hr.foi.hackathon.shift.util.Const;

/**
 * Created by David on 2.6.2014..
 */
public class SpinnerItem {

    @SerializedName(Const.ITEM_ID)
    private long id;
    @SerializedName(Const.ITEM_NAME)
    private String label;

    public SpinnerItem(long id, String label) {
        this.id = id;
        this.label = label;
    }

    public static SpinnerItem[] convertToItems(String[] items) {
        SpinnerItem[] spinnerItems = new SpinnerItem[items.length];
        for (int i = 0; i < spinnerItems.length; i++) {
            spinnerItems[i] = new SpinnerItem(i, items[i]);
        }

        return spinnerItems;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
