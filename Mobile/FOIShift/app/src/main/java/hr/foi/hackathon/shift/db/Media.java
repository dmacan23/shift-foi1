package hr.foi.hackathon.shift.db;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by ahuskano on 6/2/2014.
 */
@Table(name="Media")
public class Media extends Model {

    @Column(name="realID")
    private long realID;

    @Column(name="mediaTypeId")
    private MediaType mediaTypeId;

    @Column(name="filePath")
    private String filePath;

    public Media(long realID, MediaType mediaTypeId, String filePath) {
        this.realID = realID;
        this.mediaTypeId = mediaTypeId;
        this.filePath = filePath;
    }

    public Media() {
    }

    public long getRealID() {
        return realID;
    }

    public void setRealID(long realID) {
        this.realID = realID;
    }

    public MediaType getMediaTypeId() {
        return mediaTypeId;
    }

    public void setMediaTypeId(MediaType mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
