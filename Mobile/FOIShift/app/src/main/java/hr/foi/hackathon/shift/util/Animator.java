package hr.foi.hackathon.shift.util;

import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.internal.nineoldandroids.animation.ValueAnimator;

import hr.foi.hackathon.shift.R;

/**
 * Created by David on 2.6.2014..
 */
public class Animator {

    private ValueAnimator mAnimator;

    public void addAction(final View layout, final View summary) {

        if (summary.getVisibility() != View.GONE)
            collapse(summary);

        layout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (summary.getVisibility() == View.GONE) {
                    expand(summary);
                } else {
                    collapse(summary);
                }
            }
        });
    }

    private void expand(View summary) {
        //set Visible
        summary.setVisibility(View.VISIBLE);

        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);

        int height = 300;
        switch (summary.getId()) {
            case R.id.rlFinancialDetailsContainer:
                height = 250;
                break;
            case R.id.llNumericalDetailsContainer:
                height = 250;
                break;
            case R.id.llSpinnersContainer:
                height = 920;
                break;
        }

        summary.measure(widthSpec, height);

        mAnimator = slideAnimator(0, height, summary);

        mAnimator.start();
    }

    private void collapse(final View summary) {
        int finalHeight = summary.getHeight();

        ValueAnimator mAnimator = slideAnimator(finalHeight, 0, summary);

        mAnimator.addListener(new com.actionbarsherlock.internal.nineoldandroids.animation.Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(com.actionbarsherlock.internal.nineoldandroids.animation.Animator animator) {

            }

            @Override
            public void onAnimationEnd(com.actionbarsherlock.internal.nineoldandroids.animation.Animator animator) {
                summary.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(com.actionbarsherlock.internal.nineoldandroids.animation.Animator animator) {

            }

            @Override
            public void onAnimationRepeat(com.actionbarsherlock.internal.nineoldandroids.animation.Animator animator) {

            }
        });
        mAnimator.start();
    }


    private ValueAnimator slideAnimator(int start, int end, final View summary) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);


        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = summary.getLayoutParams();
                layoutParams.height = value;
                summary.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }
}
