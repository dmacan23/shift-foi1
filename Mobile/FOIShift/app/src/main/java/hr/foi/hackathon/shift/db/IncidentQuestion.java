package hr.foi.hackathon.shift.db;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by ahuskano on 6/2/2014.
 */
@Table(name="IncidentQuestion")
public class IncidentQuestion {

    @Column(name="answer")
    private String answer;

    @Column(name="incidentId")
    private Incident incidentId;

    @Column(name="questionId")
    private Question questionId;

    public IncidentQuestion(String answer, Incident incidentId, Question questionId) {
        this.answer = answer;
        this.incidentId = incidentId;
        this.questionId = questionId;
    }

    public IncidentQuestion() {
    }

    public Question getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Question questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Incident getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(Incident incidentId) {
        this.incidentId = incidentId;
    }
}
