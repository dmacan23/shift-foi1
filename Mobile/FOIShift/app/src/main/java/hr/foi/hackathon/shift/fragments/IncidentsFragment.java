package hr.foi.hackathon.shift.fragments;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.foi.hackathon.shift.IncidentDetailActivity;
import hr.foi.hackathon.shift.R;
import hr.foi.hackathon.shift.adapters.IncidentsAdapter;
import hr.foi.hackathon.shift.network.IncidentsAPI;
import hr.foi.hackathon.shift.types.DenisResponse;
import hr.foi.hackathon.shift.types.IncidentResponse;
import hr.foi.hackathon.shift.util.Const;
import hr.foi.hackathon.shift.util.Data;
import hr.foi.hackathon.shift.util.Session;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by David on 2.6.2014..
 */
public class IncidentsFragment extends SherlockFragment implements AdapterView.OnItemClickListener {
    private static final int RESULT_SPEECH = 1;
    public static long incident;
    private IncidentsAPI incidentsAPI;
    private ProgressDialog dialog;
    private ListView listIncidents;
    private List<IncidentResponse> responses;
    private IncidentsAdapter adapter;
    private Callback<IncidentResponse[]> callback = new Callback<IncidentResponse[]>() {
        @Override
        public void success(IncidentResponse[] incidentResponse, Response response) {
            dialog.dismiss();
            Data.incidentResponse = incidentResponse;
            responses = Arrays.asList(incidentResponse);
            adapter = new IncidentsAdapter(getSherlockActivity(), R.layout.list_item_incident, responses);
            listIncidents.setAdapter(adapter);
        }

        @Override
        public void failure(RetrofitError error) {
            dialog.dismiss();
            toastIt("ERROR: " + error.getMessage());
        }
    };

    private void init() {
        listIncidents = (ListView) getView().findViewById(R.id.listIncidents);
        listIncidents.setOnItemClickListener(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_incidents, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        if(Data.incidentResponse==null) {

            dialog = new ProgressDialog(getSherlockActivity());
            dialog.setMessage("Loading...");
            dialog.show();
            getIncidents();
        }else{
            responses = Arrays.asList(Data.incidentResponse);
            adapter = new IncidentsAdapter(getSherlockActivity(), R.layout.list_item_incident, responses);
            listIncidents.setAdapter(adapter);

        }
    }

    private void getIncidents() {
        RestAdapter adapter = new RestAdapter.Builder().setServer(Const.URL_MAIN).build();
        incidentsAPI = adapter.create(IncidentsAPI.class);
        incidentsAPI.postIncidents(new DenisResponse(Session.ID), callback);
    }

    private void toastIt(String message) {
        Toast.makeText(getSherlockActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        incident = responses.get(position).getIncidentId();
        startActivity(new Intent(getSherlockActivity(), IncidentDetailActivity.class));
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        final SearchView searchView = new SearchView(getSherlockActivity().getSupportActionBar().getThemedContext());
        searchView.setQueryHint("Search");
        menu.add(Menu.NONE, Menu.NONE, 1, "Search")
                .setIcon(R.drawable.abs__ic_search)
                .setActionView(searchView)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 0) {
                    adapter.getFilter().filter(newText);
                } else {
                    adapter.getFilter().filter("");
                }
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {

                InputMethodManager imm = (InputMethodManager) getSherlockActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);

                getSherlockActivity().setSupportProgressBarIndeterminateVisibility(true);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        getSherlockActivity().setSupportProgressBarIndeterminateVisibility(false);
                    }
                }, 2000);

                return false;
            }
        });
    }

    void testSTT() {
        Intent intent = new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");

        try {
            startActivityForResult(intent, RESULT_SPEECH);
        } catch (ActivityNotFoundException a) {
            Toast t = Toast.makeText(getSherlockActivity(),
                    "Opps! Your device doesn't support Speech to Text",
                    Toast.LENGTH_SHORT);
            t.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == getSherlockActivity().RESULT_OK && null != data) {

                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    adapter.getFilter().filter(text.get(0));
                }
                break;
            }

        }
    }
}
