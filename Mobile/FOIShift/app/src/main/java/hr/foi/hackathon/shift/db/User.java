package hr.foi.hackathon.shift.db;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by ahuskano on 6/2/2014.
 */
@Table(name="User")
public class User extends Model{
    @Column(name="realID")
    private long realID;

    @Column(name="username")
    private String username;

    @Column(name="pwd")
    private String pwd;

    @Column(name="locationId")
    private Location locationId;

    @Column(name="emailAddress")
    private String emailAddress;

    @Column(name="phoneNumber")
    private String phoneNUmber;

    public long getRealID() {
        return realID;
    }

    public void setRealID(long realID) {
        this.realID = realID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Location getLocationId() {
        return locationId;
    }

    public void setLocationId(Location locationId) {
        this.locationId = locationId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNUmber() {
        return phoneNUmber;
    }

    public void setPhoneNUmber(String phoneNUmber) {
        this.phoneNUmber = phoneNUmber;
    }

    public User() {
    }

    public User(long realID, String username, String pwd, Location locationId, String emailAddress, String phoneNUmber) {
        this.realID = realID;
        this.username = username;
        this.pwd = pwd;
        this.locationId = locationId;
        this.emailAddress = emailAddress;
        this.phoneNUmber = phoneNUmber;
    }
}
