package hr.foi.hackathon.shift.types;

import com.google.gson.annotations.SerializedName;

import hr.foi.hackathon.shift.util.Const;

/**
 * Created by David on 2.6.2014..
 */
public class UserResponse {

    @SerializedName(Const.KEY_USER_ID)
    private long id;
    @SerializedName(Const.KEY_USER_USERNAME)
    private String username;
    @SerializedName(Const.KEY_USER_PASSWORD)
    private String password;

    public UserResponse() {
    }

    public UserResponse(long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
