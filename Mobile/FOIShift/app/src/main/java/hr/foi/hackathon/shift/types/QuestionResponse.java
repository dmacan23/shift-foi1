package hr.foi.hackathon.shift.types;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahuskano on 6/3/2014.
 */
public class QuestionResponse {

    @SerializedName("QuestionId")
    private long questionId;


    @SerializedName("Name")
    private String name;

    @SerializedName("Answer")
    private int answer;

    public int getAnswer() {
        return answer;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
