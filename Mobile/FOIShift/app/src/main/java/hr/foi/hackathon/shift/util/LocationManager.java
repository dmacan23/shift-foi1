package hr.foi.hackathon.shift.util;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by David on 3.6.2014..
 */
public class LocationManager {

    private List<Address> addresses;
    private double latitude;
    private double longitude;
    private Context context;

    public LocationManager(Context context, double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.context = context;
        addresses = getAddresses();
    }

    private List<Address> getAddresses() {
        List<Address> addresses = new ArrayList<Address>();
        if (Geocoder.isPresent()) {
            Geocoder gcd = new Geocoder(context, Locale.getDefault());
            try {
                addresses = gcd.getFromLocation(latitude, longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return addresses;
    }

    public String getCity() {
        if (addresses.size() > 0)
            return addresses.get(0).getLocality();
        return "Not located";
    }

    public String getCountry() {
        if (addresses.size() > 0)
            return addresses.get(0).getCountryName();
        return "Not located";
    }

}
