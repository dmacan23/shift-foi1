package hr.foi.hackathon.shift.types;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahuskano on 6/3/2014.
 */
public class AnswerMobileResponse {

    @SerializedName("IncidentId")
    private long incidentId;

    @SerializedName("QuestionId")
    private long questionId;

    @SerializedName("Answer")
    private long answer;

    public long getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(long incidentId) {
        this.incidentId = incidentId;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public long getAnswer() {
        return answer;
    }

    public void setAnswer(long answer) {
        this.answer = answer;
    }

    public AnswerMobileResponse(long incidentId, long questionId, long answer) {
        this.incidentId = incidentId;
        this.questionId = questionId;
        this.answer = answer;
    }
}
