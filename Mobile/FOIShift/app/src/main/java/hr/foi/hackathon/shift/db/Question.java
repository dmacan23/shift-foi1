package hr.foi.hackathon.shift.db;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by ahuskano on 6/2/2014.
 */
@Table(name = "Question")
public class Question extends Model {
    @Column(name = "realID")
    private long realID;

    @Column(name = "name")
    private String name;

    @Column(name="questionCategoryId")
    private QuestionCategory questionCategoryId;

    @Column(name="active")
    private boolean active;

    public long getRealID() {
        return realID;
    }

    public void setRealID(long realID) {
        this.realID = realID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public QuestionCategory getQuestionCategoryId() {
        return questionCategoryId;
    }

    public void setQuestionCategoryId(QuestionCategory questionCategoryId) {
        this.questionCategoryId = questionCategoryId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Question() {
    }

    public Question(long realID, String name, QuestionCategory questionCategoryId, boolean active) {
        this.realID = realID;
        this.name = name;
        this.questionCategoryId = questionCategoryId;
        this.active = active;
    }
}
