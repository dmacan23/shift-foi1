package hr.foi.hackathon.shift.util;

/**
 * Created by David on 2.6.2014..
 */
public class Const {

    /**
     * URLs
     */

    public static final String URL_ANSWERS="/one/incidents/setquestions";
    public static final String URL_QUESTIONS="/one/incidents/questions";
    public static final String URL_POST = "/one/incident";
    public static final String URL_LOGIN_POST = "/one/mlogin";
    public static final String URL_GET = "/one/incident";
    public static final String URL_MAIN = "http://107.170.60.198";
    public static final String URL_SI_POST = "/one/incident/insert";
    public static final String URL_CATEGORIES_GET = "/one/categories";
    public static final String URL_LOCATIONS_GET = "/one/locations";
    public static final String URL_OUTCOMES_GET = "/one/outcome";
    public static final String URL_VIEWLVL_GET = "/one/viewlevel";
    public static final String URL_TYPES = "/one/type";
    public static final String URL_INCIDENT_DETAILS = "/one/incident/details";

    public static final int SPLASH_TIMEOUT = 1000;


    /**
     * JSON KEYS
     */

    // Incidents
    public static final String KEY_INC_USERID = "UserId";
    public static final String KEY_INC_ID = "Id";
    public static final String KEY_INC_CATEGORY_PRIM = "CatNameFirst";
    public static final String KEY_INC_CATEGORY_SEC = "CatNameSecond";
    public static final String KEY_INC_LABEL = "Title";
    public static final String KEY_INC_LOCATION = "City";

    // Login
    public static final String KEY_USER_USERNAME = "username";
    public static final String KEY_USER_PASSWORD = "password";
    public static final String KEY_USER_ID = "id";

    // Incident submit
    public static final String SI_TITLE = "Title";
    public static final String SI_DESC = "Description";
    public static final String SI_COST = "IncidentLossCost";
    public static final String SI_CURRENCY = "currency";
    public static final String SI_COST_DESC = "IncidentLossDescription";
    public static final String SI_MC_INJ = "NumberOfIMCInjuries";
    public static final String SI_NMC_INJ = "NumberOfNonIMCInjuries";
    public static final String SI_MC_DTH = "NumberOfIMDeaths";
    public static final String SI_NMC_DTH = "NumberOfNonIMCDeaths";
    public static final String SI_VIEW_LVL = "ViewLevelId";
    public static final String SI_PRIM_CAT = "PrimaryCategoryId";
    public static final String SI_SEC_CAT = "SecondaryCategoryId";
    public static final String SI_OUTCOME = "OutcomeId";
    public static final String SI_USERID = "UserId";
    public static final String SI_LATTITUDE = "LatitudeDD";
    public static final String SI_LONGITUDE = "LongitudeDD";
    public static final String SI_TYPE = "TypeId";
    public static final String SI_COUNTRY = "Country";
    public static final String SI_CITY = "City";

    public static final String ITEM_ID = "Id";
    public static final String ITEM_NAME = "Name";

    //Response
    public static final String SUCCESS = "success";



}
