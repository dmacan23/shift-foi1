package hr.foi.hackathon.shift;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.actionbarsherlock.app.SherlockActivity;

import hr.foi.hackathon.shift.util.Const;

/**
 * Created by David on 2.6.2014..
 */
public class SplashActivity extends SherlockActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splash();
    }


    private void splash() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = null;
                i = new Intent(getBaseContext(), LoginActivity.class);
                //i = new Intent(getBaseContext(), SubmitIncidentActivity.class);
                startActivity(i);
                finish();
            }
        }, Const.SPLASH_TIMEOUT);
    }
}
