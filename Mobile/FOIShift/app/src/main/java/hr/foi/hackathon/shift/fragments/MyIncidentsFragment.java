package hr.foi.hackathon.shift.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;

import java.util.ArrayList;
import java.util.List;

import hr.foi.hackathon.shift.IncidentDetailActivity;
import hr.foi.hackathon.shift.R;
import hr.foi.hackathon.shift.adapters.IncidentsAdapter;
import hr.foi.hackathon.shift.types.IncidentResponse;
import hr.foi.hackathon.shift.util.Data;
import hr.foi.hackathon.shift.util.Session;

/**
 * Created by David on 2.6.2014..
 */
public class MyIncidentsFragment extends SherlockFragment implements AdapterView.OnItemClickListener {

    private List<IncidentResponse> response2;

    private ListView listIncidents;
    private List<IncidentResponse> responses;
    private IncidentsAdapter adapter;

    private void init() {
        parseList();
        listIncidents = (ListView) getView().findViewById(R.id.listIncidents2);
        listIncidents.setOnItemClickListener(this);
        //     responses = Arrays.asList(Data.incidentResponse);
        adapter = new IncidentsAdapter(getSherlockActivity(), R.layout.list_item_incident, response2);
        listIncidents.setAdapter(adapter);
    }


    private void parseList() {
        if (response2 == null) {
            response2 = new ArrayList<IncidentResponse>();
            for (int i = 0; i < Data.incidentResponse.length; i++) {

                if (Data.incidentResponse[i].getUserId() == Session.ID) {
                    response2.add(Data.incidentResponse[i]);
                }
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_incidents_2, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        IncidentsFragment.incident = response2.get(position).getIncidentId();
        startActivity(new Intent(getSherlockActivity(), IncidentDetailActivity.class));

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        final SearchView searchView = new SearchView(getSherlockActivity().getSupportActionBar().getThemedContext());
        searchView.setQueryHint("Search");
        menu.add(Menu.NONE, Menu.NONE, 1, "Search")
                .setIcon(R.drawable.abs__ic_search)
                .setActionView(searchView)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 0) {
                    adapter.getFilter().filter(newText);
                } else {
                    adapter.getFilter().filter("");
                }
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {

                InputMethodManager imm = (InputMethodManager) getSherlockActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);

                getSherlockActivity().setSupportProgressBarIndeterminateVisibility(true);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        getSherlockActivity().setSupportProgressBarIndeterminateVisibility(false);
                    }
                }, 2000);

                return false;
            }
        });
    }


}

