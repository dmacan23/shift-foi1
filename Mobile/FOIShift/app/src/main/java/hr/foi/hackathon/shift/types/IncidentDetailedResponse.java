package hr.foi.hackathon.shift.types;

import com.google.gson.annotations.SerializedName;

/**
 * Created by David on 3.6.2014..
 */
public class IncidentDetailedResponse {

    @SerializedName("incidentId")
    private long incidentId;

    public IncidentDetailedResponse(long incidentId) {
        this.incidentId = incidentId;
    }

    public long getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(long incidentId) {
        this.incidentId = incidentId;
    }
}
