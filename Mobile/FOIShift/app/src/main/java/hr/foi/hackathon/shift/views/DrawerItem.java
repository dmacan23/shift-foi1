package hr.foi.hackathon.shift.views;

/**
 * Created by David on 2.6.2014..
 */
public class DrawerItem {

    private int iconRes;
    private String label;

    public DrawerItem(int iconRes, String label) {
        this.iconRes = iconRes;
        this.label = label;
    }

    public int getIconRes() {
        return iconRes;
    }

    public void setIconRes(int iconRes) {
        this.iconRes = iconRes;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
