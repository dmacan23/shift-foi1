package hr.foi.hackathon.shift.network;

import hr.foi.hackathon.shift.types.AnswerMobileResponse;
import hr.foi.hackathon.shift.types.AnswerResponse;
import hr.foi.hackathon.shift.types.DenisResponse;
import hr.foi.hackathon.shift.types.DetailedIncidentResponse;
import hr.foi.hackathon.shift.types.IncidentDetailedResponse;
import hr.foi.hackathon.shift.types.IncidentResponse;
import hr.foi.hackathon.shift.types.QuestionMobileResponse;
import hr.foi.hackathon.shift.types.QuestionResponse;
import hr.foi.hackathon.shift.types.SubmitIncident;
import hr.foi.hackathon.shift.types.SubmitResponse;
import hr.foi.hackathon.shift.types.UserGet;
import hr.foi.hackathon.shift.types.UserResponse;
import hr.foi.hackathon.shift.util.Const;
import hr.foi.hackathon.shift.views.SpinnerItem;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by David on 2.6.2014..
 */
public interface IncidentsAPI {


    @POST(Const.URL_POST)
    void postIncidents(@Body DenisResponse response, Callback<IncidentResponse[]> callback);

    @GET(Const.URL_GET)
    void getIncidents(Callback<IncidentResponse[]> callback);

    @POST(Const.URL_LOGIN_POST)
    void login(@Body UserResponse response, Callback<DenisResponse> callback);

    @POST(Const.URL_SI_POST)
    void submitIncident(@Body SubmitIncident submitIncident, Callback<SubmitResponse> callback);

    @POST(Const.URL_CATEGORIES_GET)
    void getCategories(@Body UserGet user, Callback<SpinnerItem[]> callback);

    @POST(Const.URL_OUTCOMES_GET)
    void getOutcomes(@Body UserGet user, Callback<SpinnerItem[]> callback);

    @POST(Const.URL_VIEWLVL_GET)
    void getViewLevels(@Body UserGet user, Callback<SpinnerItem[]> callback);

    @POST(Const.URL_TYPES)
    void getTypes(@Body UserGet user, Callback<SpinnerItem[]> callback);

    @POST(Const.URL_INCIDENT_DETAILS)
    void getIncidentDetailed(@Body IncidentDetailedResponse response, Callback<DetailedIncidentResponse> callback);

    @POST(Const.URL_QUESTIONS)
    void getQuestions(@Body QuestionMobileResponse response, Callback<QuestionResponse[]> callback);

    @POST(Const.URL_ANSWERS)
    void getAnswers(@Body AnswerMobileResponse response, Callback<AnswerResponse> callback);

}
