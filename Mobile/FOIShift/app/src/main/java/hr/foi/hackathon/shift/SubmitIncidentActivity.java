package hr.foi.hackathon.shift;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.squareup.okhttp.OkHttpClient;

import java.util.Arrays;

import hr.foi.hackathon.shift.adapters.SpinnerAdapter;
import hr.foi.hackathon.shift.network.IncidentsAPI;
import hr.foi.hackathon.shift.types.SubmitIncident;
import hr.foi.hackathon.shift.types.SubmitResponse;
import hr.foi.hackathon.shift.types.UserGet;
import hr.foi.hackathon.shift.util.Animator;
import hr.foi.hackathon.shift.util.Const;
import hr.foi.hackathon.shift.util.Helper;
import hr.foi.hackathon.shift.util.ImageHelper;
import hr.foi.hackathon.shift.util.LocationManager;
import hr.foi.hackathon.shift.util.Session;
import hr.foi.hackathon.shift.views.SpinnerItem;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by David on 2.6.2014..
 */
public class SubmitIncidentActivity extends SherlockActivity implements View.OnClickListener {
    private String imgB64;
    private String imgPath;

    private ProgressDialog progressDialog;

    private String putanja = Environment.getExternalStorageDirectory()
            .toString();
    private IncidentsAPI incidentsAPI;
    private EditText etTitle;
    private EditText etDescription;
    private EditText etIncidentCost;
    private EditText etIncidentCostDescription;
    private EditText etIncidentMCInjuries;
    private EditText etIncidentNMCInjuries;
    private EditText etIncidentMCDeaths;
    private EditText etIncidentNMCDeaths;

    private Dialog dialog;

    private ImageView imgPhoto;

    private Spinner spViewLevel;
    private Spinner spPrimaryCategory;
    private Spinner spSecondaryCategory;
    private Spinner spOutcome;
    private Spinner spCurrency;
    private Spinner spTypes;

    private Button btnSubmit;

    private SpinnerItem[] currencies;
    private SpinnerItem[] categories;
    private SpinnerItem[] viewLevels;
    private SpinnerItem[] outcomes;
    private Callback<SpinnerItem[]> outcomesCallback = new Callback<SpinnerItem[]>() {
        @Override
        public void success(SpinnerItem[] spinnerItem, Response response) {
            outcomes = spinnerItem;
            closeProgressDialog();
            setupAdapters();
        }

        @Override
        public void failure(RetrofitError error) {
            closeProgressDialog();
            toastIt("An error has occured");
        }
    };
    private SpinnerItem[] types;
    /**
     * CALLBACKS
     */
    private Callback<SubmitResponse> callback = new Callback<SubmitResponse>() {
        @Override
        public void success(SubmitResponse submitResponse, Response response) {
            closeProgressDialog();
        }

        @Override
        public void failure(RetrofitError error) {
            closeProgressDialog();
            toastIt("An error has occured: " + error.getMessage());
        }
    };
    private UserGet usr;
    private Callback<SpinnerItem[]> typesCallback = new Callback<SpinnerItem[]>() {
        @Override
        public void success(SpinnerItem[] spinnerItems, Response response) {
            types = spinnerItems;
            incidentsAPI.getOutcomes(usr, outcomesCallback);
        }

        @Override
        public void failure(RetrofitError error) {
            closeProgressDialog();
            toastIt("An error has occured");
        }
    };
    private Callback<SpinnerItem[]> viewLevelsCallback = new Callback<SpinnerItem[]>() {
        @Override
        public void success(SpinnerItem[] spinnerItem, Response response) {
            viewLevels = spinnerItem;
            incidentsAPI.getTypes(usr, typesCallback);
        }

        @Override
        public void failure(RetrofitError error) {
            closeProgressDialog();
            toastIt("An error has occured");
        }
    };
    private Callback<SpinnerItem[]> categoriesCallback = new Callback<SpinnerItem[]>() {
        @Override
        public void success(SpinnerItem[] spinnerItem, Response response) {
            categories = spinnerItem;
            incidentsAPI.getViewLevels(usr, viewLevelsCallback);
        }

        @Override
        public void failure(RetrofitError error) {
            toastIt("An error has occured!");
            closeProgressDialog();
        }
    };

    private void init() {
        imgPhoto = (ImageView) findViewById(R.id.imgPicture);
        etTitle = (EditText) findViewById(R.id.etIncidentTitle);
        etDescription = (EditText) findViewById(R.id.etIncidentDescription);
        etIncidentCost = (EditText) findViewById(R.id.etIncidentCost);
        etIncidentCostDescription = (EditText) findViewById(R.id.etIncidentCostDescription);
        etIncidentMCInjuries = (EditText) findViewById(R.id.etIncidentMCInjuries);
        etIncidentNMCInjuries = (EditText) findViewById(R.id.etIncidentNMCInjuries);
        etIncidentMCDeaths = (EditText) findViewById(R.id.etIncidentMCDeaths);
        etIncidentNMCDeaths = (EditText) findViewById(R.id.etIncidentNMCDeaths);

        spViewLevel = (Spinner) findViewById(R.id.spViewLevel);
        spPrimaryCategory = (Spinner) findViewById(R.id.spPrimaryCategory);
        spSecondaryCategory = (Spinner) findViewById(R.id.spSecondaryCategory);
        spOutcome = (Spinner) findViewById(R.id.spOutcome);
        spCurrency = (Spinner) findViewById(R.id.spCurrency);
        spTypes = (Spinner) findViewById(R.id.spTypes);

        currencies = SpinnerItem.convertToItems(getBaseContext().getResources().getStringArray(R.array.currencies));

        TextView txtFinancialLabel = (TextView) findViewById(R.id.txtIncidentCost);
        TextView txtNumericalLabel = (TextView) findViewById(R.id.txtNumericalDetails);
        TextView txtCategorizationLabel = (TextView) findViewById(R.id.txtCategorization);

        RelativeLayout rlFinancialContainer = (RelativeLayout) findViewById(R.id.rlFinancialDetailsContainer);
        LinearLayout llNumericalContainer = (LinearLayout) findViewById(R.id.llNumericalDetailsContainer);
        LinearLayout llCategorizationContainer = (LinearLayout) findViewById(R.id.llSpinnersContainer);

        Animator animator = new Animator();
        animator.addAction(txtFinancialLabel, rlFinancialContainer);
        animator.addAction(txtNumericalLabel, llNumericalContainer);
        animator.addAction(txtCategorizationLabel, llCategorizationContainer);

        showProgressDialog("Preparing data...");
        getData();

        btnSubmit = (Button) findViewById(R.id.btnSubmitIncident);
        btnSubmit.setOnClickListener(this);
        imgPhoto.setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident_submit);
        init();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmitIncident:
                submit();
                break;
            case R.id.imgPicture:
                showDialog();
                break;
            case R.id.btnChooseCamera:
                takePhoto();
                dialog.dismiss();
                break;
            case R.id.btnChoosePicker:
                pickImage();
                dialog.dismiss();
        }
    }

    private void toastIt(String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void setupAdapters() {
        spCurrency.setAdapter(new SpinnerAdapter(getBaseContext(), R.layout.spinner_item, Arrays.asList(currencies)));
        spOutcome.setAdapter(new SpinnerAdapter(getBaseContext(), R.layout.spinner_item, Arrays.asList(outcomes)));
        spSecondaryCategory.setAdapter(new SpinnerAdapter(getBaseContext(), R.layout.spinner_item, Arrays.asList(categories)));
        spPrimaryCategory.setAdapter(new SpinnerAdapter(getBaseContext(), R.layout.spinner_item, Arrays.asList(categories)));
        spViewLevel.setAdapter(new SpinnerAdapter(getBaseContext(), R.layout.spinner_item, Arrays.asList(viewLevels)));
        spTypes.setAdapter(new SpinnerAdapter(getBaseContext(), R.layout.spinner_item, Arrays.asList(types)));
    }

    private long getItemId(Spinner spinner) {
        return ((SpinnerItem) spinner.getSelectedItem()).getId();
    }

    private String getItemLabel(Spinner spinner) {
        return ((SpinnerItem) spinner.getSelectedItem()).getLabel();
    }

    private void submit() {
        showProgressDialog("Sending...");
        SubmitIncident si = packData();
        sendIncident(si);
    }

    private SubmitIncident packData() {
        SubmitIncident si = new SubmitIncident();

        si.setUserId(Session.ID);
        si.setTitle(etTitle.getText().toString());
        si.setDescription(etDescription.getText().toString());
        si.setCostDescription(etIncidentCostDescription.getText().toString());
        si.setCurrency(getItemLabel(spCurrency));

        si.setPrimaryCategoryId(getItemId(spPrimaryCategory));
        si.setSecondaryCategoryId(getItemId(spSecondaryCategory));
        si.setOutcomeId(getItemId(spOutcome));
        si.setViewLevelId(getItemId(spViewLevel));
        si.setTypeId(getItemId(spTypes));
        si.setMedia(imgB64);
        setupLocation(si);

        try {
            si.setCost(Integer.parseInt(etIncidentCost.getText().toString()));
            si.setMcDeaths(Integer.parseInt(etIncidentMCDeaths.getText().toString()));
            si.setNmcDeaths(Integer.parseInt(etIncidentNMCDeaths.getText().toString()));
            si.setMcInjuries(Integer.parseInt(etIncidentMCInjuries.getText().toString()));
            si.setNmcInjuries(Integer.parseInt(etIncidentNMCInjuries.getText().toString()));
        } catch (Exception e) {
            toastIt("Some data was inserted in wrong format");
        }

        return si;
    }

    private void sendIncident(SubmitIncident si) {
        RestAdapter adapter = new RestAdapter.Builder().setServer(Const.URL_MAIN).build();
        incidentsAPI = adapter.create(IncidentsAPI.class);
        incidentsAPI.submitIncident(si, callback);
    }

    private void getData() {
        OkHttpClient client = new OkHttpClient();
        RestAdapter adapter = new RestAdapter.Builder().setServer(Const.URL_MAIN).setClient(new OkClient(client)).build();
        incidentsAPI = adapter.create(IncidentsAPI.class);
        usr = new UserGet(Session.ID);
        incidentsAPI.getCategories(usr, categoriesCallback);
    }


    private void pickImage() {
        ImageHelper imageHelper = new ImageHelper(this);
        imageHelper.pickImage();
    }

    private void takePhoto() {
        ImageHelper imageHelper = new ImageHelper(this);
        imageHelper.takeImage();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == 1) && (resultCode == Activity.RESULT_OK)) {
            imgPath = Helper.getPathFromPicker(data.getData(), this);
            imgB64 = Helper.decodeImage(imgPath);
            displayImage(imgB64);
        } else if ((requestCode == 2) && (resultCode == Activity.RESULT_OK)) {
            imgPath = ImageHelper.putanja;
            imgB64 = Helper.decodeImage(imgPath);
            displayImage(imgB64);
        }

    }

    private void setupLocation(SubmitIncident si) {
        LocationManager lm = new LocationManager(getBaseContext(), si.getLatitude(), si.getLongitude());
        String city = lm.getCity();
        String country = lm.getCountry();
        si.setCityName(city);
        si.setCountryName(country);
    }

    private void showDialog() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_choose);

        ImageButton btnpick = (ImageButton) dialog.findViewById(R.id.btnChoosePicker);
        ImageButton btnCamera = (ImageButton) dialog.findViewById(R.id.btnChooseCamera);

        btnpick.setOnClickListener(this);
        btnCamera.setOnClickListener(this);

        dialog.show();
    }

    private void displayImage(String b64) {
        byte[] decodedString = Base64.decode(b64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        imgPhoto.setImageBitmap(decodedByte);
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    private void closeProgressDialog() {
        progressDialog.dismiss();
    }


}
