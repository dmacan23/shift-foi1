package hr.foi.hackathon.shift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import hr.foi.hackathon.shift.R;
import hr.foi.hackathon.shift.views.SpinnerItem;

/**
 * Created by David on 2.6.2014..
 */
public class SpinnerAdapter extends BaseAdapter {

    private List<SpinnerItem> items;
    private int resId;
    private Context context;

    public SpinnerAdapter(Context context, int resId, List<SpinnerItem> items) {
        this.items = items;
        this.resId = resId;
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public SpinnerItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.spinner_item, null);

        init(position, convertView);

        return convertView;
    }

    private void init(int position, View v) {
        TextView txtSpinnerLabel = (TextView) v.findViewById(R.id.txtSpinnerItem);
        txtSpinnerLabel.setText(items.get(position).getLabel());
    }
}
