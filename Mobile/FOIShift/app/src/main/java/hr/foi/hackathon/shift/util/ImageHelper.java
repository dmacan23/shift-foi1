package hr.foi.hackathon.shift.util;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by ahuskano on 6/2/2014.
 */
public class ImageHelper {
    public static String putanja=null;
    private Activity activity;

    public ImageHelper(Activity activity){
        this.activity=activity;
    }


    public void pickImage(){
        this.activity.startActivityForResult(new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 1);
    }

    public void takeImage(){
        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        putanja=Environment.getExternalStorageDirectory().toString()+ "/" + "photo" + System.currentTimeMillis()+".jpeg";
        File datoteka = new File(putanja); // .jpeg"
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(datoteka));

        this.activity.startActivityForResult(intent, 2);
    }


}
