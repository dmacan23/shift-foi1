package hr.foi.hackathon.shift;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

import java.util.ArrayList;

import hr.foi.hackathon.shift.network.IncidentsAPI;
import hr.foi.hackathon.shift.types.DenisResponse;
import hr.foi.hackathon.shift.types.UserResponse;
import hr.foi.hackathon.shift.util.Const;
import hr.foi.hackathon.shift.util.FormValidator;
import hr.foi.hackathon.shift.util.Session;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by David on 2.6.2014..
 */
public class LoginActivity extends SherlockActivity implements View.OnClickListener {

    private static final int RESULT_SPEECH = 1;

    private EditText etUsername;
    private EditText etPassword;
    private Button btnLogin;
    private IncidentsAPI incidentsAPI;

    private View testLayout;
    private Callback<DenisResponse> callback = new Callback<DenisResponse>() {
        @Override
        public void success(DenisResponse response, Response response2) {
          //  toastIt("Login successful! " + response.getMessage());
            loginPass(response.getMessage());
        }

        @Override
        public void failure(RetrofitError error) {

            toastIt("Wrong username or password");

            //     toastIt("Login error!\n" + error.getMessage());
        }
    };

    private void init() {
        etUsername = (EditText) findViewById(R.id.etLoginUsername);
        etPassword = (EditText) findViewById(R.id.etLoginPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }

    private void login() {
        RestAdapter adapter = new RestAdapter.Builder().setServer(Const.URL_MAIN).build();
        incidentsAPI = adapter.create(IncidentsAPI.class);

        UserResponse ur = new UserResponse();
        ur.setPassword(etPassword.getText().toString());
        ur.setUsername(etUsername.getText().toString());

        incidentsAPI.login(ur, callback);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (!FormValidator.isEditTextEmpty(etUsername) && !FormValidator.isEditTextEmpty(etPassword)) {
                    login();
                }else{
                    Toast.makeText(this,"Username or password is empty",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void toastIt(String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void loginPass(long id) {
        Session.USERNAME = etUsername.getText().toString();
        Session.PASSWORD = etPassword.getText().toString();
        Session.ID = id;

       // startActivity(new Intent(getBaseContext(), QuestionActivity.class));
        startActivity(new Intent(getBaseContext(), NavigationActivity.class));
        this.finish();
    }

    void testSTT() {
        Intent intent = new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");

        try {
            startActivityForResult(intent, RESULT_SPEECH);
        } catch (ActivityNotFoundException a) {
            Toast t = Toast.makeText(getApplicationContext(),
                    "Opps! Your device doesn't support Speech to Text",
                    Toast.LENGTH_SHORT);
            t.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    Toast.makeText(this, text.get(0), Toast.LENGTH_SHORT).show();
                }
                break;
            }

        }
    }
}
