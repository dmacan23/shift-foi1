package hr.foi.hackathon.shift.types;

import com.google.gson.annotations.SerializedName;

/**
 * Created by David on 3.6.2014..
 */
public class DetailedIncidentResponse {

    @SerializedName("Id")
    private long id;
    @SerializedName("Title")
    private String title;
    @SerializedName("City")
    private String city;
    @SerializedName("CatNameFirst")
    private String catNameFirst;
    @SerializedName("CatNameSecond")
    private String catNameSecond;
    @SerializedName("Username")
    private String username;
    @SerializedName("Lat")
    private String lattitude;
    @SerializedName("Long")
    private String longitude;
    @SerializedName("Status")
    private String status;
    @SerializedName("ViewLevel")
    private String viewLevel;
    @SerializedName("Type")
    private String type;
    @SerializedName("Outcome")
    private String outcome;
    @SerializedName("FilePath")
    private String filePath;

    public DetailedIncidentResponse() {
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCatNameFirst() {
        return catNameFirst;
    }

    public void setCatNameFirst(String catNameFirst) {
        this.catNameFirst = catNameFirst;
    }

    public String getCatNameSecond() {
        return catNameSecond;
    }

    public void setCatNameSecond(String catNameSecond) {
        this.catNameSecond = catNameSecond;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getViewLevel() {
        return viewLevel;
    }

    public void setViewLevel(String viewLevel) {
        this.viewLevel = viewLevel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }
}
