package hr.foi.hackathon.shift.util;

import android.widget.EditText;

/**
 * Created by David on 2.6.2014..
 */
public class FormValidator {

    public static final int CATEGORIES = 1;
    public static final int VIEW_LVLS = 2;
    public static final int OUTCOMES = 3;

    public static int getSpinnerDataId(String url) {
        if (chop(url).equals(chop(Const.URL_CATEGORIES_GET)))
            return CATEGORIES;
        if (chop(url).equals(chop(Const.URL_OUTCOMES_GET)))
            return OUTCOMES;
        if (chop(url).equals(chop(Const.URL_VIEWLVL_GET)))
            return VIEW_LVLS;

        return 0;
    }

    public static String chop(String url) {
        String[] chop = url.split("/");
        return chop[chop.length - 1];
    }


    public static boolean isInputValid(EditText et) {
        if (!isEditTextEmpty(et))
            return true;

        return false;
    }

    public static boolean isEditTextEmpty(EditText et) {
        if (et == null)
            return true;
        if (et.getText().toString().equals(""))
            return true;

        return false;
    }
}
