package hr.foi.hackathon.shift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hr.foi.hackathon.shift.R;
import hr.foi.hackathon.shift.views.DrawerItem;

/**
 * Created by David on 2.6.2014..
 */
public class DrawerAdapter extends BaseAdapter {

    private Context context;
    private List<DrawerItem> items;
    private int resId;

    public DrawerAdapter(Context context, List<DrawerItem> items, int resId) {
        this.context = context;
        this.items = items;
        this.resId = resId;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public DrawerItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.drawer_list_item, null);
        init(position, convertView);
        return convertView;
    }

    private void init(int position, View v) {
        ImageView img = (ImageView) v.findViewById(R.id.imgDrawerItemIcon);
        TextView label = (TextView) v.findViewById(R.id.txtDrawerItemLabel);

        img.setImageResource(items.get(position).getIconRes());
        label.setText(items.get(position).getLabel());
    }
}
