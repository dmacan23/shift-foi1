package hr.foi.hackathon.shift;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import hr.foi.hackathon.shift.adapters.DrawerAdapter;
import hr.foi.hackathon.shift.fragments.IncidentsFragment;
import hr.foi.hackathon.shift.fragments.MapFragment;
import hr.foi.hackathon.shift.fragments.MyIncidentsFragment;
import hr.foi.hackathon.shift.views.DrawerItem;

/**
 * Created by David on 2.6.2014..
 */
public class NavigationActivity extends SherlockFragmentActivity {

    DrawerLayout drawerLayout;
    ListView drawerItemList;
    private List<DrawerItem> items;
    private List<SherlockFragment> fragments;
    private ActionBarDrawerToggle drawerToggle;
    private CharSequence drawerTitle;
    private CharSequence title;

    private String[] drawerItemTitles;
    private int[] drawerItemIcons;

    private DrawerAdapter adapter;

    private int frameContainer;
    private int drawerLayoutId;
    private int drawerListId;
    private int drawerIcon;
    private int drawerItemLayout;

    @Override
    protected void onCreate(Bundle args) {
        super.onCreate(args);
        setContentView(R.layout.activity_navigation);
        init();
        drawerItemList.setOnItemClickListener(new SlideMenuClickListener());
        displayView(0);
    }

    private void init() {
        initDrawerItems();
        initFragments();
        initResources();
        initElements();
        setupTitlesAndIcons();
        configureDrawerToggle();
    }

    private void initResources() {
        drawerLayoutId = R.id.drawerLayout;
        drawerListId = R.id.drawerItemList;
        drawerIcon = R.drawable.ic_navigation_drawer;
        frameContainer = R.id.frameContainer;
        drawerItemLayout = R.layout.list_item_incident;
    }

    private void initElements() {
        this.title = this.drawerTitle = getTitle();
        drawerLayout = (DrawerLayout) findViewById(drawerLayoutId);
        drawerItemList = (ListView) findViewById(drawerListId);
        adapter = new DrawerAdapter(getBaseContext(), items,
                drawerItemLayout);
        drawerItemList.setAdapter(adapter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void configureDrawerToggle() {
        drawerToggle = new ActionBarDrawerToggle(this, this.drawerLayout,
                drawerIcon, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerClosed(View drawerView) {
                getSupportActionBar().setTitle(drawerTitle);
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(title);
                supportInvalidateOptionsMenu();
            }
        };
        this.drawerLayout.setDrawerListener(drawerToggle);
    }

    private void initDrawerItems() {
        items = new ArrayList<DrawerItem>();
        items.add(new DrawerItem(R.drawable.incidents, "Incidents"));
        items.add(new DrawerItem(R.drawable.incidents, "My incidents"));
        items.add(new DrawerItem(R.drawable.icon_map, "Incident map"));
    }

    private void initFragments() {
        fragments = new ArrayList<SherlockFragment>();
        fragments.add(new IncidentsFragment());
        fragments.add(new MyIncidentsFragment());
        fragments.add(new MapFragment());
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.main, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (drawerLayout.isDrawerOpen(drawerItemList)) {
                    drawerLayout.closeDrawer(drawerItemList);
                } else {
                    drawerLayout.openDrawer(drawerItemList);
                }
                return true;
            case R.id.menuNewIncident:
                startActivity(new Intent(getBaseContext(), SubmitIncidentActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private boolean displayView(int position) {
        SherlockFragment fragment = null;

        if (position < fragments.size()) {
            fragment = fragments.get(position);

            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(frameContainer, fragment)
                    .commit();
            drawerItemList.setItemChecked(position, true);
            drawerItemList.setSelection(position);
            setTitle(drawerItemTitles[position]);
            drawerLayout.closeDrawer(drawerItemList);
            return true;
        }
        return false;
    }

    private void setupTitlesAndIcons() {
        drawerItemTitles = new String[items.size()];
        drawerItemIcons = new int[items.size()];
        for (int i = 0; i < items.size(); i++) {
            drawerItemTitles[i] = items.get(i).getLabel();
            drawerItemIcons[i] = items.get(i).getIconRes();
        }
    }

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            displayView(position);
        }

    }
}
