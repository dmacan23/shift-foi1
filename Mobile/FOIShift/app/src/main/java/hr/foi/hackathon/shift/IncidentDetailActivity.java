package hr.foi.hackathon.shift;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.squareup.picasso.Picasso;

import hr.foi.hackathon.shift.fragments.IncidentsFragment;
import hr.foi.hackathon.shift.network.IncidentsAPI;
import hr.foi.hackathon.shift.types.DetailedIncidentResponse;
import hr.foi.hackathon.shift.types.IncidentDetailedResponse;
import hr.foi.hackathon.shift.util.Const;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by David on 3.6.2014..
 */
public class IncidentDetailActivity extends SherlockActivity {
    private IncidentsAPI incidentsAPI;
    private TextView txtIncDetTitle, tvCategory, txtIncDetCity, txtUsername, txtType, txtOutcome, txtViewLevel, tvStatus;
    private ImageView img;
    private Callback<DetailedIncidentResponse> callback = new Callback<DetailedIncidentResponse>() {
        @Override
        public void success(DetailedIncidentResponse submitIncident, Response response) {
            txtIncDetCity.setText(submitIncident.getCity());
            tvCategory.setText(submitIncident.getCatNameFirst());
            txtIncDetTitle.setText(submitIncident.getTitle());
            txtUsername.setText(submitIncident.getUsername());
            txtViewLevel.setText(submitIncident.getViewLevel());
            txtType.setText(submitIncident.getType());
            txtOutcome.setText(submitIncident.getOutcome());
            tvStatus.setText(submitIncident.getStatus());
            dialog.dismiss();
            Picasso.with(getBaseContext()).load(submitIncident.getFilePath()).placeholder(R.drawable.ic_launcher).into(img);
        }

        @Override
        public void failure(RetrofitError error) {
            dialog.dismiss();
            toastIt("Error: " + error.getMessage());
        }
    };
    private ProgressDialog dialog;

    private void init() {
        txtIncDetCity = (TextView) findViewById(R.id.txtIncDetCity);
        tvCategory = (TextView) findViewById(R.id.tvCategory);
        txtIncDetTitle = (TextView) findViewById(R.id.txtIncDetTitle);
        txtUsername = (TextView) findViewById(R.id.txtUsername);
        txtUsername = (TextView) findViewById(R.id.txtUsername);
        txtType = (TextView) findViewById(R.id.txtUsername);
        txtOutcome = (TextView) findViewById(R.id.txtOutcome);
        txtViewLevel = (TextView) findViewById(R.id.txtViewLevel);
        tvStatus = (TextView) findViewById(R.id.txtStatus);
        img = (ImageView) findViewById(R.id.img);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident_detail);
        init();

        RestAdapter adapter = new RestAdapter.Builder().setServer(Const.URL_MAIN).build();
        incidentsAPI = adapter.create(IncidentsAPI.class);
        IncidentDetailedResponse idr = new IncidentDetailedResponse(IncidentsFragment.incident);
        incidentsAPI.getIncidentDetailed(idr, callback);
    }

    private void toastIt(String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }
}
