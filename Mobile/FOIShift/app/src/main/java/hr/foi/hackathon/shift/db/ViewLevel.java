package hr.foi.hackathon.shift.db;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by ahuskano on 6/2/2014.
 */
@Table(name="ViewLevel")
public class ViewLevel extends Model {

    @Column(name="realID")
    private long realID;

    @Column(name="name")
    private String name;

    @Column(name="description")
    private String description;

    public ViewLevel(long realID, String name, String description) {
        this.realID = realID;
        this.name = name;
        this.description = description;
    }

    public ViewLevel() {
    }

    public long getRealID() {
        return realID;
    }

    public void setRealID(long realID) {
        this.realID = realID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
