package hr.foi.hackathon.shift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import hr.foi.hackathon.shift.R;
import hr.foi.hackathon.shift.types.QuestionResponse;

/**
 * Created by ahuskano on 6/3/2014.
 */
public class QuestionAdapter extends BaseAdapter{

    private Context context;
    private int resId;
    private List<QuestionResponse> items;

    public QuestionAdapter(Context context, int resId, List<QuestionResponse> items) {
        this.context = context;
        this.resId = resId;
        this.items = items;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public QuestionResponse getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_item_question, null);
        init(position, convertView);
        return convertView;
    }

    private void init(int position, View v) {
        TextView txtQuestion = (TextView) v.findViewById(R.id.tvQuestion);
        txtQuestion.setText(items.get(position).getName());


    }
}
