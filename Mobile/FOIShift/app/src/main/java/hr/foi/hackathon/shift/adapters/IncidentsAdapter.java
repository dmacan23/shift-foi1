package hr.foi.hackathon.shift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import hr.foi.hackathon.shift.R;
import hr.foi.hackathon.shift.types.IncidentResponse;

/**
 * Created by David on 2.6.2014..
 */
public class IncidentsAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private int resId;
    private List<IncidentResponse> items;
    private List<IncidentResponse> originalItems;
    private int lastPosition = -1;

    public IncidentsAdapter(Context context, int resId, List<IncidentResponse> items) {
        this.context = context;
        this.resId = resId;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public IncidentResponse getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_item_incident, null);

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;

        init(position, convertView);
        return convertView;
    }

    private void init(int position, View v) {
        TextView txtLabel = (TextView) v.findViewById(R.id.txtIncidentLabel);
        TextView txtCategory = (TextView) v.findViewById(R.id.txtIncidentCategory);
        TextView txtLocation = (TextView) v.findViewById(R.id.txtIncidentLocation);
        txtLabel.setText(items.get(position).getIncidentLabel());
        txtCategory.setText(items.get(position).getCategoryPrimLabel());
        txtLocation.setText(items.get(position).getLocationLabel());
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<IncidentResponse> filteredArray = new ArrayList<IncidentResponse>();

                if (originalItems == null) {
                    originalItems = new ArrayList<IncidentResponse>(items);
                }
                if (constraint == null || constraint.length() == 0) {
                    results.count = originalItems.size();
                    results.values = originalItems;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < originalItems.size(); i++) {
                        IncidentResponse ir = originalItems.get(i);
                        if (ir.getIncidentLabel().toLowerCase().startsWith(constraint.toString())) {
                            filteredArray.add(ir);
                        }
                    }

                    results.count = filteredArray.size();
                    System.out.println(results.count);

                    results.values = filteredArray;
                }

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                items = (List<IncidentResponse>) results.values;
                notifyDataSetChanged();
            }
        };

        return filter;
    }


}
