package hr.foi.hackathon.shift.types;

import com.google.gson.annotations.SerializedName;

/**
 * Created by David on 2.6.2014..
 */
public class SubmitResponse {

    @SerializedName("message")
    private String response;

    public SubmitResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
