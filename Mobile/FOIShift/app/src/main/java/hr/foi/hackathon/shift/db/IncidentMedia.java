package hr.foi.hackathon.shift.db;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by ahuskano on 6/2/2014.
 */
@Table(name="IncidentMedia")
public class IncidentMedia {

    @Column(name="incidentId")
    private Incident incidentId;

    @Column(name="mediaId")
    private Media mediaId;

    public IncidentMedia(Incident incidentId, Media mediaId) {
        this.incidentId = incidentId;
        this.mediaId = mediaId;
    }

    public IncidentMedia() {
    }

    public Incident getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(Incident incidentId) {
        this.incidentId = incidentId;
    }

    public Media getMediaId() {
        return mediaId;
    }

    public void setMediaId(Media mediaId) {
        this.mediaId = mediaId;
    }
}
