package hr.foi.hackathon.shift;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.foi.hackathon.shift.adapters.QuestionAdapter;
import hr.foi.hackathon.shift.network.IncidentsAPI;
import hr.foi.hackathon.shift.types.AnswerMobileResponse;
import hr.foi.hackathon.shift.types.AnswerResponse;
import hr.foi.hackathon.shift.types.QuestionMobileResponse;
import hr.foi.hackathon.shift.types.QuestionResponse;
import hr.foi.hackathon.shift.util.Const;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ahuskano on 6/3/2014.
 */
public class QuestionActivity extends SherlockActivity implements AdapterView.OnItemClickListener {
    private IncidentsAPI incidentsAPI;

    private ProgressDialog dialog;
    private ListView listQuestion;

    private List<QuestionResponse> responses;
    private Callback<QuestionResponse[]> callback = new Callback<QuestionResponse[]>() {
        @Override
        public void success(QuestionResponse[] questionResponse, Response response) {
            dialog.dismiss();
            responses = Arrays.asList(questionResponse);
            listQuestion.setAdapter(new QuestionAdapter(getBaseContext(), R.layout.list_item_question, responses));
        }

        @Override
        public void failure(RetrofitError error) {
            dialog.dismiss();
            toastIt("ERROR: " + error.getMessage());
        }
    };
    private Callback<AnswerResponse> callbackAnswer = new Callback<AnswerResponse>() {

        @Override
        public void success(AnswerResponse answerResponse, Response response) {

        }

        @Override
        public void failure(RetrofitError error) {
          //  dialog.dismiss();
            toastIt("ERROR: " + error.getMessage());
        }
    };

    private void toastIt(String message) {
        Toast.makeText(QuestionActivity.this, message, Toast.LENGTH_SHORT).show();
    }


    private void init() {
        listQuestion = (ListView) findViewById(R.id.listQuestion);
        listQuestion.setOnItemClickListener(QuestionActivity.this);

        dialog = new ProgressDialog(QuestionActivity.this);
        dialog.setMessage("Loading...");
        dialog.show();

        responses = new ArrayList<QuestionResponse>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        init();
        getIncidents();
    }

    private void getIncidents() {
        RestAdapter adapter = new RestAdapter.Builder().setServer(Const.URL_MAIN).build();
        incidentsAPI = adapter.create(IncidentsAPI.class);
        incidentsAPI.getQuestions(new QuestionMobileResponse(1), callback);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, final int position, long ld) {
        final Dialog answer = new Dialog(QuestionActivity.this);
        answer.setTitle("Answer this question");
        answer.setContentView(R.layout.dialog_answer);
        TextView title = (TextView) answer.findViewById(R.id.tvTitle);
        title.setText(responses.get(position).getName());
        final RadioGroup radioGroup = (RadioGroup) answer.findViewById(R.id.rbAnswer);
        if (responses.get(position).getAnswer() == 1) {
            radioGroup.check(R.id.radioTrue);
        } else {
            radioGroup.check(R.id.radioFalse);
        }
        Button zovi = (Button) answer.findViewById(R.id.btSubmit);
        zovi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                int id = radioGroup.getCheckedRadioButtonId();
                if (id == R.id.radioTrue) {
                    id = 1;
                } else {
                    id = 0;
                }
                RestAdapter adapter = new RestAdapter.Builder().setServer(Const.URL_ANSWERS).build();
                incidentsAPI = adapter.create(IncidentsAPI.class);
                incidentsAPI.getAnswers(new AnswerMobileResponse(1,responses.get(position).getQuestionId(),id), callbackAnswer);
                answer.dismiss();
            }
        });
        answer.show();

    }
}
