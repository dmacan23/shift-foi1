package hr.foi.hackathon.shift.types;

import com.google.gson.annotations.SerializedName;

import hr.foi.hackathon.shift.util.Const;

/**
 * Created by David on 2.6.2014..
 */
public class UserGet {

    @SerializedName(Const.KEY_INC_USERID)
    private long userId;

    public UserGet(long userId) {
        this.userId = userId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
