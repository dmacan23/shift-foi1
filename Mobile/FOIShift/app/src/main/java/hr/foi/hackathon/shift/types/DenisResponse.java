package hr.foi.hackathon.shift.types;

import com.google.gson.annotations.SerializedName;

/**
 * Created by David on 2.6.2014..
 */
public class DenisResponse {

    @SerializedName("UserId")
    private long message;

    public DenisResponse(long message) {
        this.message = message;
    }

    public long getMessage() {
        return message;
    }

    public void setMessage(long message) {
        this.message = message;
    }
}
