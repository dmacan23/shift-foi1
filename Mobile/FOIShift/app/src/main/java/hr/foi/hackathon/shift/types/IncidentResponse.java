package hr.foi.hackathon.shift.types;

import com.google.gson.annotations.SerializedName;

import hr.foi.hackathon.shift.util.Const;

/**
 * Created by David on 2.6.2014..
 */
public class IncidentResponse {

    @SerializedName(Const.KEY_INC_USERID)
    private long userId;
    @SerializedName(Const.KEY_INC_ID)
    private long incidentId;
    @SerializedName(Const.KEY_INC_LABEL)
    private String incidentLabel;
    @SerializedName(Const.KEY_INC_CATEGORY_PRIM)
    private String categoryPrimLabel;
    @SerializedName(Const.KEY_INC_CATEGORY_SEC)
    private String categorySecLabel;
    @SerializedName(Const.KEY_INC_LOCATION)
    private String locationLabel;
    @SerializedName("Lat")
    private double Lat;
    @SerializedName("Long")
    private double Lng;

    public double getLat() {
        return Lat;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public double getLng() {
        return Lng;
    }

    public void setLng(double lng) {
        Lng = lng;
    }

    public IncidentResponse(long userId, long incidentId, String incidentLabel, String categoryPrimLabel, String categorySecLabel, String locationLabel, double lat, double lng) {
        this.userId = userId;
        this.incidentId = incidentId;
        this.incidentLabel = incidentLabel;
        this.categoryPrimLabel = categoryPrimLabel;
        this.categorySecLabel = categorySecLabel;
        this.locationLabel = locationLabel;
        Lat = lat;
        Lng = lng;
    }

    public IncidentResponse() {
    }

    public IncidentResponse(long userId, long incidentId, String incidentLabel, String categoryPrimLabel, String categorySecLabel, String locationLabel) {
        this.userId = userId;
        this.incidentId = incidentId;
        this.incidentLabel = incidentLabel;
        this.categoryPrimLabel = categoryPrimLabel;
        this.categorySecLabel = categorySecLabel;
        this.locationLabel = locationLabel;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(long incidentId) {
        this.incidentId = incidentId;
    }

    public String getIncidentLabel() {
        return incidentLabel;
    }

    public void setIncidentLabel(String incidentLabel) {
        this.incidentLabel = incidentLabel;
    }

    public String getCategoryPrimLabel() {
        return categoryPrimLabel;
    }

    public void setCategoryPrimLabel(String categoryPrimLabel) {
        this.categoryPrimLabel = categoryPrimLabel;
    }

    public String getCategorySecLabel() {
        return categorySecLabel;
    }

    public void setCategorySecLabel(String categorySecLabel) {
        this.categorySecLabel = categorySecLabel;
    }

    public String getLocationLabel() {
        return locationLabel;
    }

    public void setLocationLabel(String locationLabel) {
        this.locationLabel = locationLabel;
    }

}
