package hr.foi.hackathon.shift.types;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahuskano on 6/3/2014.
 */
public class QuestionMobileResponse {
    @SerializedName("incidentId")
    private long incidentId;


    public QuestionMobileResponse(long incidentId) {
        this.incidentId = incidentId;
    }

    public long getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(long incidentId) {
        this.incidentId = incidentId;
    }
}
